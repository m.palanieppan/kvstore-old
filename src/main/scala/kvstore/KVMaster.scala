package kvstore

import java.security.MessageDigest

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import java.util.TreeMap

import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.ExceptionHandler

import scala.io.StdIn
import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.concurrent.{Await, ExecutionContext}
import scala.util.{Failure, Success}

/**
  * Created by pala on 4/26/17.
  */
object KVMaster {
  val ring = new ConsistentHashRing()
  val nodeSet = new mutable.TreeSet[Int]()

  def main(args: Array[String]): Unit = {
    // interface includes:
    // 1. listen to node registration and assign key ranges
    // 2. Monitor node heartbeats and track node aliveness
    // 3. Supply the node map information to clients, to allow them to query the store

    implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher

    val exceptionHandler = ExceptionHandler {
      case ex:Exception => {
        extractUri {
          uri => complete(HttpResponse(StatusCodes.InternalServerError, entity = ex.toString()))
        }
      }
    }

    val route =
      handleExceptions(exceptionHandler) {
        post {
          path("addNode") {
            entity(as[String]) { value =>
              addNode(value.toInt)
              complete("Node added")
            }
          }
        } ~
          get {
            pathPrefix("getNode" / Segment) { key =>
              complete(getNode(key).toString)
            }
          } ~
          get {
            pathPrefix("getRingMap") {
              complete(serializeRing)
            }
          }
      }

    val port = Consts.MasterPortNumber
    val bindingFuture = Http().bindAndHandle(route, "localhost", port)

    // schedule background task to check known nodes are alive
    system.scheduler.schedule(FiniteDuration(10, "s"), FiniteDuration(60, "s"))(leaseCheck)

    println(s"KVMaster server online at http://localhost:$port/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }

  def addNode(port:Int): Unit = {
    assert(!nodeSet.contains(port), s"Node on port $port is already present.")
    nodeSet.add(port)
    val portStr = port.toString
    (1 to Consts.VirtualNodeCount).foreach(vn => ring.put(portStr + vn.toString, port))
  }

  def removeNode(port: Int): Unit = {
    val portStr = port.toString
    (1 to Consts.VirtualNodeCount).foreach(vn => ring.remove(portStr + vn.toString))

    assert(nodeSet.contains(port), s"Node on port $port is not present.")
    nodeSet.remove(port)
  }

  def getNode(key:String): Int = {
    if(ring.size() == 0) {
      throw new RuntimeException("No nodes registered")
    }
    ring.getOwner(key)
  }

  def leaseCheck(implicit actorSystem: ActorSystem,
                 materializer: ActorMaterializer,
                 ec:ExecutionContext): Unit = {

    nodeSet.par.foreach( port => {
      try {
        Await.result(
          Http().singleRequest(HttpRequest(uri = s"http://localhost:$port/heartbeat"))
            .andThen {
              case Failure(_) => println(s"Removing node $port"); removeNode(port)
              case Success(_) =>
            },
          atMost = Duration(1000, "ms"))
      } catch {
        // unhandled exception outside of http flow, in that case remove node also
        // we get this after a node is not connectible, and the node is removed above

        case _:Throwable => println(s"Unhandled exception for $port.");
      }
    })
  }
}
