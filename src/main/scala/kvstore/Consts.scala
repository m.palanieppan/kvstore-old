package kvstore

/**
  * Created by pala on 4/28/17.
  */
object Consts {
  val ClientPortNumber = 8999
  val MasterPortNumber = 9000
  // Number of virtual nodes for each physical node
  val VirtualNodeCount = 100
}
