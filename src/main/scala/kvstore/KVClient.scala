package kvstore


import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.concurrent.duration.Duration

/**
  * Created by pala on 5/18/17.
  */
class KVClient()(implicit val actorSystem:ActorSystem, val materializer:ActorMaterializer,
                 val dispatcher:ExecutionContextExecutor) {

  val ring = refreshRingData()

  def putKey(key:String, value:String) = {
    val ownerPort = ring.getOwner(key)
    val resp = Http().singleRequest(HttpRequest(uri = s"http://localhost:$ownerPort/putKey/$key",
      method = HttpMethods.POST, entity = HttpEntity(value))).result(Duration(1L, "s"))
  }

  def getKey(key:String): Option[String] = {
    val ownerPort = ring.getOwner(key)
    val httpResponse = Http().singleRequest(HttpRequest(uri = s"http://localhost:$ownerPort/getKey/$key",
      method = HttpMethods.GET)).result(Duration(1L, "s"))

    val oneSecDuration = Duration(1L, "s")
    httpResponse.status match {
      case StatusCodes.NotFound => None
      case _ => Some(
        httpResponse.toStrict(oneSecDuration)
          .map(s => s.entity).result(oneSecDuration).toStrict(oneSecDuration)
          .map(s => s.data).result(oneSecDuration).utf8String
      )
    }
  }

  def refreshRingData() = {
    val masterPort = Consts.MasterPortNumber
    val ringMapResponseFuture = Http().singleRequest(HttpRequest(
      uri = s"http://localhost:$masterPort/addNode", method = HttpMethods.GET))

    val ringMapResponse = ringMapResponseFuture.result(Duration(1L, "s"))
    val data = ringMapResponse.entity.toStrict(Duration(1L, "s")).map(s => s.data).result(Duration(1L, "s")).utf8String

    ConsistentHashRing.deserialize(data)
  }
}
